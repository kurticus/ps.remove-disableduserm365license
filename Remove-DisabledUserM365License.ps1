<#
.SYNOPSIS
	Removes all users licenses from a disabled user
.DESCRIPTION
	This script will remove all Microsoft 365 licenses that are manually assigned to a user that is disabled
.PARAMETER Test
    Setting this switch will enable test mode where no licenses will be freed. Just the total count of licenses will be output that would have been freed.
.EXAMPLE
    Remove-DisabledUserM365License
    Remove-DisabledUserM365License -Test
.NOTES
	Author:       Kurt Marvin
	
	Changelog:
	   1.0        Initial release
#>

#region ##################### Parameters #####################
Param 
(
	[Parameter(Mandatory=$false)]
	[Switch]$Test
)
#endregion

#region ##################### Variables #####################


#endregion

#region ##################### Functions #####################


#endregion

#region ##################### Main #####################

# Check if the necessary modules exist
if (-not(Get-Module -ListAvailable -Name "MSOnline")) {
    Write-Warning "The necessary module of MsolService is not installed. Please install the MSOnline module by running 'Install-Module MSOnline' in an elevated PS prompt."    
    exit 1
}

if ($Test) { Write-Warning "Running in 'Test' mode. No licenses will be freed." }

# Connect to Azure AD
Connect-MsolService

# Get all terminated users that have a license assigned
Write-Host "Getting the list of users from Azure AD."
$terminatedLicensedUsers = Get-MsolUser -All | Where {$_.isLicensed -eq $true -and $_.BlockCredential -eq $true}

foreach ($terminatedUser in $terminatedLicensedUsers)
{
    $licenses = @()
    $terminatedUser.Licenses | foreach {
        $licenses += $_.AccountSkuId
    }   
    
    if (-not($Test)) {
        Set-MsolUserLicense -UserPrincipalName $terminatedUser.UserPrincipalName -RemoveLicenses $licenses
        Write-Host "Removing license from $($terminatedUser.UserPrincipalName)"
    }
}

Write-Host "Total Terminated Users with Licenses: $($terminatedLicensedUsers.Count)"

#endregion